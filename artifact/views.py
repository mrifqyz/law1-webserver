from django.shortcuts import render
import requests
import json

# Create your views here.
def index(request):
    if request.GET['a'] and request.GET['b']:
        data = {
            'a': int(request.GET['a']),
            'b': int(request.GET['b'])
        }
        rq = requests.post("http://localhost:8080/add", json=data)
        data = json.loads(rq.content)
        return render(request, "index.html", data)